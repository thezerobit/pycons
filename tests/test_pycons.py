"""
Test pycons
"""

import unittest
from pycons import (nil, cons, toclist, clist, consp, car, cdr, first, rest,
                    null, clistp)

class Test_cons(unittest.TestCase):
    def setUp(self):
        pass

    def test_nil(self):
        self.assertTrue(nil == nil)
        self.assertTrue(nil is nil)
        self.assertFalse("a" in nil)
        self.assertTrue(clist() is nil)
        self.assertTrue(toclist([]) is nil)
        with self.assertRaises(IndexError):
            nil[0]
        self.assertEqual(0, len(nil))
        for item in nil:
            self.assertFalse("should never get here")
        with self.assertRaises(ValueError):
            nil.index("a")
        self.assertEqual(0, nil.count("a"))
        self.assertEqual("nil", str(nil))

    def test_cons(self):
        self.assertTrue(consp(cons("a", nil)))
        self.assertTrue("a" in cons("a", nil))
        self.assertFalse("b" in cons("a", nil))
        self.assertEqual(cons("b", nil), cons("b", nil))
        self.assertEqual(cons("b", cons("a", nil)), cons("b", cons("a", nil)))
        self.assertNotEqual(cons("a", nil), cons("b", nil))
        self.assertNotEqual(cons("b", cons("b", nil)),
                            cons("b", cons("a", nil)))

    def test_clist(self):
        self.assertFalse(consp(clist()))
        self.assertTrue(consp(clist("a")))
        self.assertEqual(clist("a", "b"), cons("a", cons("b", nil)))
        self.assertEqual(repr(clist('a', 'b')), "clist('a', 'b')")

    def test_car(self):
        self.assertEqual("a", car(clist("a")))
        self.assertEqual(nil, car(clist()))

    def test_cdr(self):
        self.assertEqual(nil, cdr(clist("a")))
        self.assertEqual(clist("b"), cdr(clist("a", "b")))
        self.assertEqual(nil, cdr(clist()))

    def test_first(self):
        self.assertEqual("a", first(clist("a")))
        self.assertEqual(nil, first(clist()))

    def test_rest(self):
        self.assertEqual(nil, rest(clist("a")))
        self.assertEqual(clist("b"), rest(clist("a", "b")))
        self.assertEqual(nil, rest(clist()))

    def test_null(self):
        self.assertTrue(null(nil))
        self.assertTrue(null(clist()))
        self.assertFalse(null(clist("a")))

    def test_toclist(self):
        abc_clist = clist("a", "b", "c")
        self.assertEqual(abc_clist, toclist(["a", "b", "c"]))
        # have to reconvert back to set to compare unordered values
        self.assertEqual(frozenset(abc_clist),
                         frozenset(toclist(frozenset(["a", "b", "c"]))))

    def test_improper_clist(self):
        improper = clist(1, 2, 3, tail=4)
        self.assertEqual(improper, cons(1, cons(2, cons(3, 4))))
        self.assertEqual("clist(1, 2, 3, tail=4)", repr(improper))
        # iterating over an improper list is an error
        with self.assertRaises(ValueError):
            foo = [x for x in improper]

