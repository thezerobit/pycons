# -*- coding: utf-8 -*-

from setuptools import setup, find_packages

with open('README.rst') as f:
    readme = f.read()

with open('requirements.txt') as f:
    requirements = f.readlines()
    install_requires = [line for line in requirements if not line.startswith('#')]

with open('requirements-testing.txt') as f:
    test_reqs = f.readlines()
    tests_require = [line for line in test_reqs if not line.startswith('#')]

setup(
    name='pycons',
    version='0.1',
    description='Immutable Linked Lists',
    long_description=readme,
    author='Stephen A. Goss',
    packages=find_packages(exclude=('tests', 'docs')),
    install_requires = install_requires,
    tests_require = tests_require,
)
