"""
PyCons - Immutable Lisp style linked list.
"""

import collections

class ListIter(object):
    """Class that acts as an iterator for any clist or nil.
    """
    def __init__(self, lst):
        self.lst = lst
    def __iter__(self):
        return self
    def next(self):
        if self.lst is nil:
            raise StopIteration
        if not consp(self.lst):
            raise ValueError("The value " + repr(self.lst) + " is not a clist")
        fst, self.lst = self.lst.car(), self.lst.cdr()
        return fst

class NilClass(collections.Sequence):
    """Basic empty list implements sequences methods for nil (the empty clist).
    """
    def car(self):
        return nil

    def cdr(self):
        return nil

    def __getitem__(self, key):
        raise IndexError

    def __len__(self):
        return 0

    def __contains__(self, item):
        return False

    def __iter__(self):
        return ListIter(nil)

    def __reversed__(self):
        return ListIter(nil)

    def index(self, item):
        raise ValueError("%s is not in clist")

    def count(self, item):
        return 0

    def __repr__(self):
        return "nil"

    def __hash__(self):
        return id(self)

nil = NilClass()
NilClass.__init__ = None # there can be only 1 nil

class cons(collections.Sequence):
    """The classic cons cell from which all clists are built.
    """
    def __init__(self, car, cdr):
        self.__car = car
        self.__cdr = cdr

    def car(self):
        return self.__car

    def cdr(self):
        return self.__cdr

    def __getitem__(self, key):
        i = 0
        for item in self:
            if i == key:
                return item
            i = i + 1
        raise IndexError

    def __len__(self):
        i = 0
        for item in self:
            i = i + 1
        return i

    def __contains__(self, item):
        for t in self:
            if t == item:
                return True
        return False

    def __iter__(self):
        return ListIter(self)

    def __reversed__(self):
        revlist = nil
        for item in self:
            revlist = cons(item, revlist)
        return revlist.__iter__()

    def index(self, item):
        index = 0
        for thing in self:
            if item == thing:
                return index
            index += 1
        raise ValueError("%s is not in clist")

    def count(self, item):
        count = 0
        for thing in self:
            if item == thing:
                count += 1
        return count

    def __repr__(self):
        tail = self
        rep = "clist("
        while consp(tail):
            rep += repr(car(tail))
            tail = cdr(tail)
            if tail is not nil:
                rep += ", "
        if tail is not nil:
            rep += "tail=" + repr(tail)
        rep += ")"
        return rep

    def __eq__(self, other):
        a, b = self, other
        while True:
            if consp(a) and consp(b):
                if car(a) != car(b):
                    return False
                a, b = cdr(a), cdr(b)
            elif (consp(a) and not consp(b)) or (not consp(a) and consp(b)):
                return False
            else:
                return a == b

    def __ne__(self, other):
        return not self.__eq__(other)

def car(cell):
    return cell.car()

def first(cell):
    return cell.car()

def cdr(cell):
    return cell.cdr()

def rest(cell):
    return cell.cdr()

def consp(seq):
    return isinstance(seq, cons)

def null(seq):
    return seq is nil

def clistp(seq):
    return null(seq) or consp(seq)

def toclist(seq, tail=nil):
    """Converts a sequence to a linked list.
    """
    newlist = tail
    if isinstance(seq, collections.Sequence):
        for item in reversed(seq):
            newlist = cons(item, newlist)
    elif isinstance(seq, collections.Iterable):
        for item in seq:
            newlist = cons(item, newlist)
    return newlist

def clist(*args, **kwargs):
    """Converts args to a linked list.
    """
    return toclist(args, **kwargs)




