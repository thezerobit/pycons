pycons - Immutable Linked Lists
===============================

These are Lisp style linked lists that happen to be immutable. For certain
operations, like creating a new list by appending (consing) values to the
front, this datatype will be more efficient than tuples. Immutability is
nice, too. The lists (clist) implement the complete collections.Sequence
interface and are compatible with itertools and everything else in Python
that consumes sequences.

Usage
-----

::

  >>> from pycons import (clist, cons, car, cdr)
  >>> myclist = clist(1, 2, 3, 4)
  >>> anotherlist = cons(0, myclist)
  >>> myclist
  clist(1, 2, 3, 4)
  >>> anotherlist
  clist(0, 1, 2, 3, 4)
  >>> myclist[0] = -1
  Traceback (most recent call last):
    File "<stdin>", line 1, in <module>
  TypeError: 'cons' object does not support item assignment
  >>> car(myclist)
  1
  >>> cdr(myclist)
  clist(2, 3, 4)




